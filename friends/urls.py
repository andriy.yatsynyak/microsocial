# coding=utf-8
from django.conf.urls import url

from friends.views import FriendsView, IncomingView, OutcomingView, __action_user

urlpatterns = [
    url(r'^action_users/$', __action_user, name='action_users'),
    url(r'^incoming/$', IncomingView.as_view(), name='incoming'),
    url(r'^outcoming/$', OutcomingView.as_view(), name='outcoming'),
    url(r'', FriendsView.as_view(), name='friends'),
]
