# coding=utf-8
from user.views import paginator
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from user.models import User

# remove_friend = id
# add_friend = id
# confirm = id
# reject = id
# cancel = id


def __action_user(request, *args, **kwargs):
    if 'add_friend' in request.POST and request.POST['add_friend']:
        if User.managers.check_application(request.user, request.POST['add_friend']):
            messages.error(request, _(u'Заявка была отправлена еще раньше.'))
            return redirect(request.environ['HTTP_REFERER'])
        res = User.managers.add_friendship(request.user, request.POST['add_friend'])
        if res:
            messages.success(request, _(u'Пользователь успешно добавлен в друзья.'))
        elif res is False:
            messages.success(request, _(u'Заявка успешно отправлена и ожидает рассмотрения.'))
        else:
            messages.error(request, _(u'you dont add friend about youself'))
    elif 'remove_friend' in request.POST and request.POST['remove_friend']:
        User.managers.del_friendship(request.user, request.POST['remove_friend'])
        messages.success(request, _(u'Пользователь успешно удален из друзей.'))
    elif 'confirm' in request.POST and request.POST['confirm']:
        if User.managers.check_application(request.POST['confirm'], request.user):
            res = User.managers.add_friendship(request.user, request.POST['confirm'])
            if res:
                messages.success(request, _(u'Заявка успешно подтверджена. Пользователь добавлен в друзья.'))
            else:
                messages.error(request, _(u'something wrong to confirm friends'))
    elif 'reject' in request.POST and request.POST['reject']:
        User.managers.del_application(request.POST['reject'], request.user)
        messages.success(request, _(u'Заявка успешно отклонена.'))
    elif 'cancel' in request.POST and request.POST['cancel']:
        User.managers.del_application(request.user, request.POST['cancel'])
        messages.success(request, _(u'Заявка успешно отменена.'))
    return redirect(request.environ['HTTP_REFERER'])


class FriendsView(TemplateView):
    template_name = 'friends/friends.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        self.page = request.GET.get('page', None)
        return super(FriendsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FriendsView, self).get_context_data(**kwargs)
        qs = self.user.friends.all()
        context['friends'] = paginator(qs, 20, self.page)
        return context


class IncomingView(TemplateView):
    template_name = 'friends/incoming.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super(IncomingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IncomingView, self).get_context_data(**kwargs)
        qs = self.user.sent_applications.all()
        context['friends'] = paginator(qs, 20, self.request.GET.get('page'))
        return context


class OutcomingView(TemplateView):
    template_name = 'friends/outcoming.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super(OutcomingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OutcomingView, self).get_context_data(**kwargs)
        qs = self.user.application.all()
        context['friends'] = paginator(qs, 20, self.request.GET.get('page'))
        return context
