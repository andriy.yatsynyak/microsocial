from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User, Wall, PostWall


class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('email', 'first_name', 'is_staff')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'password1', 'password2')
        }),
    )
    fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password')
        }),
        ('Personal info', {
           'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'sex', 'birthday', 'city', 'work_place', 'about', 'interests')
        }),
        ('Permissions', {
            'classes': ('wide',),
            'fields': ('is_active', 'is_staff', 'is_superuser')
        }),
        ('Important dates', {
            'classes': ('wide',),
            'fields': ('last_login', 'date_joined')
        }),
    )

    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


admin.site.register(User, CustomUserAdmin)

admin.site.register(PostWall)
admin.site.register(Wall)
