# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import user.models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0016_auto_20171230_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='about',
            field=models.CharField(max_length=255, null=True, verbose_name='\u043e\u0431\u043e \u043c\u043d\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(upload_to=user.models.avatar_upload_to, verbose_name='\u0430\u0432\u0430\u0442\u0430\u0440', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='birthday',
            field=models.DateField(null=True, verbose_name='\u0434\u0435\u043d\u044c \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='city',
            field=models.CharField(max_length=50, null=True, verbose_name='\u0433\u043e\u0440\u043e\u0434', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='date_joined',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0434\u0430\u0442\u0430 \u043b\u043e\u0433\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(max_length=150, verbose_name='\u0438\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='interests',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0439'),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_staff',
            field=models.BooleanField(default=False, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(max_length=150, null=True, verbose_name='\u0444\u0430\u043c\u0438\u043b\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='sex',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='\u043f\u043e\u043b', choices=[('M', '\u043c\u0443\u0436\u0447\u0438\u043d\u0430'), ('W', '\u0436\u0435\u043d\u0449\u0438\u043d\u044b')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='work_place',
            field=models.CharField(max_length=100, null=True, verbose_name='\u043c\u0435\u0441\u0442\u043e \u0440\u0430\u0431\u043e\u0442\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='wall',
            name='action',
            field=models.CharField(max_length=100, verbose_name='\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435', choices=[('1', '\u043f\u043e\u0434\u0440\u0443\u0436\u0438\u043b\u0438\u0441\u044c'), ('2', '\u0440\u0430\u0437\u043e\u0440\u0432\u0430\u043b\u0438 \u0434\u0440\u0443\u0436\u0431\u0443'), ('3', '\u043d\u0430\u043f\u0438\u0441\u0430\u043b \u043d\u0430 \u0441\u0432\u043e\u0435\u0439 \u0441\u0442\u0435\u043d\u0435'), ('4', '\u043d\u0430\u043f\u0438\u0441\u0430\u043b \u043d\u0430 \u0441\u0442\u0435\u043d\u0435')]),
        ),
    ]
