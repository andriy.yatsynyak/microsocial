# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0005_auto_20171204_1016'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='send_application',
        ),
        migrations.AddField(
            model_name='user',
            name='application',
            field=models.ManyToManyField(related_name='_user_application_+', verbose_name='\u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u0437\u0430\u044f\u0432\u043a\u0443', to=settings.AUTH_USER_MODEL),
        ),
    ]
