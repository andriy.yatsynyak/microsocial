# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0015_auto_20171230_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wall',
            name='action',
            field=models.CharField(max_length=100, verbose_name='\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435', choices=[(1, '\u043f\u043e\u0434\u0440\u0443\u0436\u0438\u043b\u0438\u0441\u044c'), (2, '\u0440\u0430\u0437\u043e\u0440\u0432\u0430\u043b\u0438 \u0434\u0440\u0443\u0436\u0431\u0443'), (3, '\u043d\u0430\u043f\u0438\u0441\u0430\u043b \u043d\u0430 \u0441\u0432\u043e\u0435\u0439 \u0441\u0442\u0435\u043d\u0435'), (4, '\u043d\u0430\u043f\u0438\u0441\u0430\u043b \u043d\u0430 \u0441\u0442\u0435\u043d\u0435')]),
        ),
    ]
