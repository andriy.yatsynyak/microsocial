# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_auto_20171130_1028'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostWall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_data', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0439')),
                ('post', models.TextField(max_length=1000, verbose_name='\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
            ],
            options={
                'ordering': ('post_data',),
            },
        ),
        migrations.AddField(
            model_name='user',
            name='friends',
            field=models.ManyToManyField(related_name='_user_friends_+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='postwall',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='postwall',
            name='receiver',
            field=models.ForeignKey(related_name='post_messages', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterIndexTogether(
            name='postwall',
            index_together=set([('receiver', 'post_data')]),
        ),
    ]
