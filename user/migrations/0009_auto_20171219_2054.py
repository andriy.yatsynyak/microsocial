# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_auto_20171205_1230'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dialog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('author', models.ForeignKey(related_name='authors', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-messages__time_create',),
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(max_length=1000, verbose_name='\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f')),
                ('time_create', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0439')),
            ],
            options={
                'ordering': ('-time_create',),
            },
        ),
        migrations.AddField(
            model_name='dialog',
            name='messages',
            field=models.ForeignKey(related_name='messages_re', to='user.Message'),
        ),
        migrations.AddField(
            model_name='dialog',
            name='recipient',
            field=models.ForeignKey(related_name='recipient_for', to=settings.AUTH_USER_MODEL),
        ),
    ]
