# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0010_auto_20171227_1420'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wall',
            name='action',
            field=models.CharField(max_length=5, verbose_name='\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435', choices=[(1, 'F'), (2, 'NF'), (3, 'WY'), (4, 'WOU')]),
        ),
    ]
