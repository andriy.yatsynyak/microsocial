# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_auto_20171205_0245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='application',
            field=models.ManyToManyField(related_name='sent_applications', verbose_name='\u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u0437\u0430\u044f\u0432\u043a\u0443', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='friends',
            field=models.ManyToManyField(related_name='_user_friends_+', verbose_name='\u0434\u0440\u0443\u0437\u044c\u044f', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
