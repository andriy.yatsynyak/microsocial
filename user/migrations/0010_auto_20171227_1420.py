# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20171219_2054'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u0435')),
                ('action', models.CharField(max_length=20, verbose_name='\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435', choices=[(1, 'F'), (2, 'NF'), (3, 'WY'), (4, 'WOU')])),
                ('messages_wall', models.ForeignKey(blank=True, to='user.PostWall', null=True)),
                ('user1', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('user2', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.AddField(
            model_name='user',
            name='wall',
            field=models.ManyToManyField(related_name='_user_wall_+', verbose_name='\u0441\u0442\u0435\u043d\u0430', to='user.Wall'),
        ),
    ]
