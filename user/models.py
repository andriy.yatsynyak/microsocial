# coding=utf-8
import os
import datetime
from boltons.setutils import IndexedSet
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from user.signals import create_wall_action_friends


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, password=None):
        if not email:
            raise ValueError(ugettext(u'Пользователи должны иметь адрес электронной почты'))
        elif not first_name:
            raise ValueError(ugettext(u'Пользователи должны иметь имя'))
        if User.objects.filter(email=self.normalize_email(email)).exists():
            raise ValueError(ugettext(u'Пользователь с такой электронной почте зарегистрирован'))
        user = self.model(email=self.normalize_email(email), first_name=first_name)
        user.set_password(password)
        user.is_staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, password):
        user = self.create_user(email, password=password, first_name=first_name)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class FriendsManager(models.Manager):

    def _get_pk_users(self, user, user_to):
        user_pk = user.pk if isinstance(user, User) else user
        user_to_pk = user_to.pk if isinstance(user_to, User) else user_to
        return int(user_pk), int(user_to_pk)

    def are_friends(self, user, user_to):
        user_pk, friend_pk = self._get_pk_users(user, user_to)
        return User.objects.filter(pk=user_pk, friends=friend_pk).exists()

    def add_friendship(self, user, friend):
        user_pk, friend_pk = self._get_pk_users(user, friend)
        if user_pk != friend_pk:
            user = User.objects.get(pk=user_pk)
            friend = User.objects.get(pk=friend_pk)
            qs = User.objects.filter(pk=friend_pk, application=user_pk)
            if qs.exists():
                user.friends.add(friend)
                create_wall_action_friends.send(None, user=user, friend=friend, action='post_add')
                user.application.remove(friend)
                friend.application.remove(user)
                return True
            user.application.add(friend)
            return False
        return

    def check_application(self, user, friend):
        user_pk, friend_pk = self._get_pk_users(user, friend)
        return User.objects.filter(pk=user_pk, application=friend_pk).exists()

    def del_friendship(self, user, friend):
        user_pk, friend_pk = self._get_pk_users(user, friend)
        usr = User.objects.get(pk=user_pk)
        friend = User.objects.get(pk=friend_pk)
        create_wall_action_friends.send(None, user=user, friend=friend, action='pre_remove')
        usr.friends.remove(friend)

    def del_application(self, user_from, user_to):
        user_pk, user_to_pk = self._get_pk_users(user_from, user_to)
        usr = User.objects.get(pk=user_pk)
        usr.application.remove(user_to_pk)


def avatar_upload_to(instance, filename):
    return os.path.join('img', '{}{}'.format(instance.pk, os.path.splitext(filename)[1]))


class User(AbstractBaseUser, PermissionsMixin):
    MEN = u'M'
    WOMEN = u'W'
    SEX = (
        (MEN, _(u'мужчина')),
        (WOMEN, _(u'женщины')),
        )
    avatar = models.ImageField(_(u'аватар'), upload_to=avatar_upload_to, blank=True)
    email = models.EmailField(_(u'email'), unique=True)
    first_name = models.CharField(_(u'имя'), max_length=150)
    last_name = models.CharField(_(u'фамилия'), max_length=150, blank=True, null=True)
    sex = models.CharField(_(u'пол'), choices=SEX, max_length=1, blank=True, null=True)
    birthday = models.DateField(_(u'день рождения'), blank=True, null=True)
    city = models.CharField(_(u'город'), max_length=50, blank=True, null=True)
    work_place = models.CharField(_(u'место работы'), max_length=100, blank=True, null=True)
    about = models.CharField(_(u'обо мне'), max_length=255, blank=True, null=True)
    interests = models.CharField(_(u'интереси'), max_length=255, blank=True, null=True)
    confirmed_registration = models.BooleanField(_(u'confirmed registration'), default=False)
    is_staff = models.BooleanField(_(u'статус пользователя'), default=False)
    is_active = models.BooleanField(_(u'активный'), default=True)
    date_joined = models.DateTimeField(_(u'дата логирования'), default=timezone.now)
    application = models.ManyToManyField('self', verbose_name=_(u'отправить заявку'), symmetrical=False,
                                         related_name='sent_applications', blank=True)
    friends = models.ManyToManyField('self', verbose_name=_(u'друзья'), blank=True, related_name='+')
    wall = models.ManyToManyField('Wall', verbose_name=_(u'стена'), related_name='+')

    objects = UserManager()
    managers = FriendsManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('first_name',)

    def get_full_name(self):
        return u'{} {}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        return u'{}'.format(self.email)

    def email_user(self, subject, message, from_email=None, **kwargs):
        return send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_age(self):
        if self.birthday:
            return int((datetime.date.today() - self.birthday).days / 365.2425)


class PostWall(models.Model):
    receiver = models.ForeignKey(User, related_name='post_messages')
    author = models.ForeignKey(User)
    post_data = models.DateTimeField(_(u'дата публикаций'), auto_now_add=True)
    post = models.TextField(_(u'сообщения'), max_length=1000)

    class Meta:
        ordering = ('-post_data',)
        index_together = ('receiver', 'post_data')


class Message(models.Model):
    text = models.TextField(_(u'сообщения'), max_length=1000)
    time_create = models.DateTimeField(_(u'дата публикаций'), auto_now_add=True)

    class Meta:
        ordering = ('-time_create',)


class DialogManager(models.Manager):
    def _get_pk_users(self, user, user_to):
        user_pk = user.pk if isinstance(user, User) else user
        user_to_pk = user_to.pk if isinstance(user_to, User) else user_to
        return int(user_pk), int(user_to_pk)

    def get_list_dialog(self, user):
        user_pk = user.pk if isinstance(user, User) else user
        users_pk = IndexedSet(i if i != user_pk else j for i, j in Dialog.get_dialog.
                              filter(Q(recipient=user_pk) | Q(author=user_pk)).select_related('author', 'recipient').
                              values_list('author_id', 'recipient_id'))
        users_pk -= {user_pk}
        dialog = []
        for pk in users_pk:
            in_user = Dialog.get_dialog.filter(author=pk, recipient=user_pk).select_related('author', 'messages').\
                only('author_id', 'messages__time_create').first()
            out_user = Dialog.get_dialog.filter(author=user_pk, recipient=pk).select_related('recipient', 'messages').\
                only('recipient_id', 'messages__time_create').first()
            if in_user and out_user:
                if (in_user.messages.time_create - out_user.messages.time_create).total_seconds() >= 0:
                    dialog.append(in_user.author_id)
                else:
                    dialog.append(out_user.recipient_id)
            else:
                tmp = getattr(in_user, 'author_id', None) or getattr(out_user, 'recipient_id', None)
                if tmp:
                    dialog.append(tmp)
        return dialog

    def get_messages(self, user, with_user):
        user, with_user = self._get_pk_users(user, with_user)
        messages = Dialog.get_dialog.filter(Q(author=user, recipient=with_user) | Q(author=with_user, recipient=user)).\
            select_related('messages', 'author').order_by('-messages__time_create')
        return messages


class Dialog(models.Model):
    author = models.ForeignKey(User, related_name='authors')
    recipient = models.ForeignKey(User, related_name='recipient_for')
    messages = models.ForeignKey(Message, related_name='messages_re', db_index=True)

    get_dialog = DialogManager()

    class Meta:
        ordering = ('-messages__time_create',)


def get_pk_users(*args):
    res = []
    for value in args:
        if value:
            res.append(value.pk if isinstance(value, User) else int(value))
    return res


class ManagerWall(models.Manager):
    def create_wall(self, action, user1, user2=None, messages=None):
        usr = get_pk_users(user1, user2)
        if len(usr) > 1:
            return Wall.objects.create(user1_id=usr[0], user2_id=usr[1], action=action, messages_wall=messages)
        return Wall.objects.create(user1_id=usr[0], action=action, messages_wall=messages)

    def get_all_pk_friends(self, pk):
        tmp = list(User.objects.filter(pk=pk).prefetch_related('friends').values_list('friends', flat=True))
        tmp.append(pk)
        return tmp

    def get_all_pk_friends_without_dublicate(self, user1, user2):
        usr_pks = get_pk_users(user1, user2)
        frineds1 = set(self.get_all_pk_friends(pk=usr_pks[0]))
        frineds2 = set(self.get_all_pk_friends(pk=usr_pks[1]))
        return frineds1 | frineds2


class Wall(models.Model):
    FRIENDSHIP = u'1'
    NOT_FRIENDSHIP = u'2'
    WROTE = u'3'
    WROTE_OTHER_USER = u'4'

    ACTION_CHOICE = (
        (FRIENDSHIP, _(u'подружились')),
        (NOT_FRIENDSHIP, _(u'разорвали дружбу')),
        (WROTE, _(u'написал на своей стене')),
        (WROTE_OTHER_USER, _(u'написал на стене')),
    )
    user1 = models.ForeignKey(User, related_name='+')
    user2 = models.ForeignKey(User, related_name='+', blank=True, null=True)
    created = models.DateTimeField(_(u'дата создание'), auto_now_add=True)
    action = models.CharField(_(u'действие'), choices=ACTION_CHOICE, max_length=100)
    messages_wall = models.ForeignKey(PostWall, blank=True, null=True)

    objects = ManagerWall()

    class Meta:
        ordering = ('-created',)


@receiver(post_save, sender=PostWall, dispatch_uid='wall_post')
def create_wall(sender, instance, **kwargs):
    if instance.receiver_id == instance.author_id:
        obj = Wall.objects.create_wall(action=Wall.WROTE, user1=instance.author, messages=instance)
        pks = Wall.objects.get_all_pk_friends(instance.author_id)
    else:
        obj = Wall.objects.create_wall(action=Wall.WROTE_OTHER_USER, user1=instance.author, user2=instance.receiver,
                                       messages=instance)
        pks = Wall.objects.get_all_pk_friends_without_dublicate(user1=instance.author, user2=instance.receiver)
    usrs = User.objects.filter(pk__in=pks)
    for usr in usrs:
        usr.wall.add(obj)


@receiver(create_wall_action_friends)
def create_wall_about_friends(sender, **kwargs):
    action = kwargs.pop('action', None)
    if action in ('pre_remove', 'post_add'):
        if action in 'pre_remove':
            obj = Wall.objects.create_wall(action=Wall.NOT_FRIENDSHIP, user1=kwargs['user'], user2=kwargs['friend'])
        elif action in 'post_add':
            obj = Wall.objects.create_wall(action=Wall.FRIENDSHIP, user1=kwargs['user'], user2=kwargs['friend'])
        pks = Wall.objects.get_all_pk_friends_without_dublicate(kwargs['user'], kwargs['friend'])
        usrs = User.objects.filter(pk__in=pks)
        for usr in usrs:
            usr.wall.add(obj)
