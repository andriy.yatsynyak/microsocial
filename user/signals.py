# coding=utf-8
from django.dispatch import Signal

create_wall_action_friends = Signal(providing_args=['user', 'friend', 'action'])
