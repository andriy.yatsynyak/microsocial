# coding=utf-8
from django.conf.urls import url
from django.contrib.auth.views import logout_then_login
from user import views
from user.views import MessagesView

urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'^registration/', views.RegistrationView.as_view(), name='registration'),
    url(r'^password-recovery/$', views.PasswordRecoveryView.as_view(), name='pass_recovery'),
    url(r'^password-recovery/(?P<token>.+)/$', views.PasswordRecoveryConfirmView.as_view()),
    url(r'^profile/(?P<user_id>\d+)/$', views.UserProfileView.as_view(), name='profile'),
    url(r'^settings/$', views.MyView.as_view(), name='settings'),
    url(r'^search/', views.SearchView.as_view(), name='search'),
    url(r'^messages/$', MessagesView.as_view(), name='messages'),
    url(r'^messages/(?P<user_id>\d+)/$', MessagesView.as_view(), name='dialog'),
    url(r'^new/$', views.WallView.as_view(), name='wall'),
]
