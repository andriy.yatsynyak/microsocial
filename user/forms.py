# coding=utf-8
from collections import OrderedDict
from datetime import datetime
from django import forms
from microsocial.form import BootstrapFormMixin
from user.models import User, PostWall, Message
from django.utils.translation import ugettext, ugettext_lazy as _


class RegistrationForm(forms.ModelForm, BootstrapFormMixin):
    password2 = forms.CharField(label='', widget=forms.PasswordInput({'placeholder': _(u'пароль')}))

    class Meta:
        model = User
        fields = ('first_name', 'email', 'password')
        labels = {'first_name': '', 'email': '', 'password': ''}
        widgets = {
            'first_name': forms.TextInput({'placeholder': _(u'имя')}),
            'email': forms.EmailInput({'placeholder': _(u'email')}),
            'password': forms.PasswordInput({'placeholder': _(u'пароль')}),
        }

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean_password2(self):
        if 'password2' in self.cleaned_data and 'password' in self.cleaned_data:
            if self.cleaned_data['password2'] != self.cleaned_data['password']:
                raise forms.ValidationError(ugettext(u'Different passwords'))

    def clean(self):
        data = self.cleaned_data
        if 'first_name' in data and 'email' in data:
            data['first_name'] = data['first_name'].strip()
            data['email'] = data['email'].strip()
            if data['first_name'] == '' or data['email'] == '':
                raise forms.ValidationError(ugettext(u'Empty some fields'))
        return data

    def save(self, commit=True):
        return User.objects.create_user(self.cleaned_data['email'], self.cleaned_data['first_name'],
                                        password=self.cleaned_data['password'])


class CustomPasswordResetForum(forms.ModelForm, BootstrapFormMixin):
    class Meta:
        model = User
        fields = ('email',)
        labels = {'email': ''}
        widgets = {
            'email': forms.TextInput({'placeholder': 'email'}),
        }

    def __init__(self, *args, **kwargs):
        super(CustomPasswordResetForum, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean(self):
        email = self.cleaned_data.get('email', '').strip()
        if email and not User.objects.filter(email=email).exists():
            self.add_error('email', ugettext(u'Пользователь с введенным email не существует'))

    def get_user(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            return User.objects.get(email=email)
        return


class UserProfile(forms.ModelForm, BootstrapFormMixin):
    avatar = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'class': 'ask-signup-avatar-input'}),
        required=False, label=_(u'аватар')
    )

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'sex', 'birthday', 'city', 'work_place', 'about', 'interests')
        widgets = {
            'about': forms.Textarea(),
            'interests': forms.Textarea(),
            'birthday': forms.TextInput({'placeholder': _(u'Введите дату в формате ГГГГ-ММ-ДД')}),
        }

    def __init__(self, *args, **kwargs):
        super(UserProfile, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean_avatar(self):
        avatar = self.cleaned_data.get('avatar')
        if avatar is not None and 'image' not in avatar.content_type:
            raise forms.ValidationError(ugettext(u'Неверный формат картинки'))
        return avatar

    def save(self, commit=True):
        if self.instance.avatar:
            User.objects.get(pk=self.instance.pk).avatar.delete()
        self.instance.avatar = self.cleaned_data.get('avatar', None)
        if commit:
            self.instance.save(update_fields=('avatar', 'first_name', 'last_name', 'sex', 'birthday', 'city',
                                              'work_place', 'about', 'interests'))
        return self.instance


class UserPasswordChangeForm(forms.Form, BootstrapFormMixin):
    error_messages = {
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    old_password = forms.CharField(label=_("Old password"), widget=forms.PasswordInput)
    new_password1 = forms.CharField(label=_("New password"), widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("New password"), widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        UserPasswordChangeForm.base_fields = OrderedDict(
            (k, UserPasswordChangeForm.base_fields[k])
            for k in ['old_password', 'new_password1', 'new_password2']
        )
        super(UserPasswordChangeForm, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data["new_password2"])
        if commit:
            self.user.save(update_fields=('password',))
        return self.user


class ChangeEmailForm(forms.ModelForm, BootstrapFormMixin):
    class Meta:
        model = User
        fields = ('email', 'password',)
        widgets = {
            'password': forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ChangeEmailForm, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        if not email:
            raise forms.ValidationError(ugettext(u'пустой email'))
        elif User.objects.filter(email=email).exists():
            raise forms.ValidationError(ugettext(u'такой пользователь зарегистрирован в системе'))
        return email

    def clean_password(self):
        password = self.cleaned_data.get('password', None)
        if not self.user.check_password(password):
            raise forms.ValidationError(ugettext(u'неверный пароль'))
        return password

    def save(self, commit=True):
        self.user.email = self.cleaned_data['email']
        if commit:
            self.user.save(update_fields=('email',))
        return self.user


class PostUserWallForm(forms.ModelForm, BootstrapFormMixin):
    class Meta:
        model = PostWall
        fields = ('post',)
        widgets = {
            'post': forms.Textarea({'placeholder': _(u'напишите на стене...'), 'rows': 8}),
        }

    def __init__(self, receiver, author, *args, **kwargs):
        self.receiver = receiver
        self.author = author
        super(PostUserWallForm, self).__init__(*args, **kwargs)
        BootstrapFormMixin.__init__(self)

    def clean_post(self):
        post = self.cleaned_data.get('post', None).strip()
        if not post:
            raise forms.ValidationError(ugettext(u'поле не можеть бить пустим'))
        return post

    def save(self, commit=True):
        self.instance.post = self.cleaned_data['post']
        self.instance.receiver = self.receiver
        self.instance.author = self.author
        if commit:
            self.instance.save()
        return self.instance


class SearchForm(forms.Form, BootstrapFormMixin):

    ALL = 'A'
    names = forms.CharField(label=_(u'Имя, Фамилия'), max_length=300,
                            widget=forms.TextInput({'placeholder': _(u'Имя, Фамилия')}), required=False)
    sex = forms.ChoiceField(label=_(u'пол'), choices=[(ALL, _(u'все')), User.SEX[0], User.SEX[1]], initial=ALL)
    start_year = forms.IntegerField(label=_(u'год рождения'), min_value=1900, widget=forms.TextInput(), required=False)
    end_year = forms.IntegerField(min_value=1900, widget=forms.TextInput(), required=False)
    city = forms.CharField(label=_(u'город'), max_length=150, required=False)
    work_place = forms.CharField(label=_(u'место работы'), max_length=200, required=False)
    about = forms.CharField(label=_(u'о себе'), max_length=200, required=False)
    interests = forms.CharField(label=_(u'интересы'), max_length=200, required=False)
    first_name = forms.CharField(label='', widget=forms.HiddenInput(), required=False)
    last_name = forms.CharField(label='', widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['start_year'].max_value = datetime.now().year
        self.fields['end_year'].max_value = datetime.now().year
        BootstrapFormMixin.__init__(self)

    def clean(self):
        start_year = self.cleaned_data.get('start_year', '')
        end_year = self.cleaned_data.get('end_year', '')
        if start_year or end_year:
            try:
                if int(start_year) < 1900 or int(end_year) < 1900:
                    raise forms.ValidationError(ugettext(u'неможет бить миньше 1900 года'))
                elif int(start_year) > datetime.now().year or int(end_year) > datetime.now().year:
                    raise forms.ValidationError(ugettext(u'неможет бить больше чем теперешний год'))
            except (ValueError, TypeError):
                raise forms.ValidationError(ugettext(u'некорректно введено года'))
        if start_year and end_year and int(start_year) > int(end_year):
            raise forms.ValidationError(ugettext(u'некорректно введено года'))
        names = self.cleaned_data.get('names', '')
        data = [c.strip() for c in names.split(' ')]
        if len(data) == 2:
            self.cleaned_data['first_name'], self.cleaned_data['last_name'] = data
        elif len(data) == 1:
            self.cleaned_data['first_name'] = self.cleaned_data['last_name'] = data[0]
        else:
            raise forms.ValidationError(ugettext(u'некорректно введен формат'))
        return self.cleaned_data


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('text',)
        labels = {'text': ''}
        widgets = {
            'text': forms.Textarea({'rows': 8, 'cols': 90}),
        }
