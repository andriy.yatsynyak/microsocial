# coding=utf-8
from django.contrib import messages
from django.contrib.auth import authenticate, login, BACKEND_SESSION_KEY
from django.contrib.auth.forms import SetPasswordForm
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.signing import Signer, BadSignature, TimestampSigner, SignatureExpired
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.utils.translation import ugettext as _
from user.forms import (RegistrationForm, CustomPasswordResetForum, UserProfile, ChangeEmailForm,
                        UserPasswordChangeForm, PostUserWallForm, SearchForm, MessageForm)
from user.models import User, PostWall, Dialog, Wall


def paginator(qs, count, page):
    paginator = Paginator(qs, count)
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)
    return contacts


class UserProfileView(TemplateView):
    template_name = 'user/profile.html'

    def dispatch(self, request, *args, **kwargs):
        self.receiver = get_object_or_404(User, pk=kwargs['user_id'])
        self.form = PostUserWallForm(self.receiver, request.user, request.POST or None)
        self.page = request.GET.get('page', None)
        return super(UserProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)
        context['profile_user'] = self.receiver
        context['form'] = self.form
        qs = PostWall.objects.filter(receiver=self.receiver).\
            select_related('author').only('post', 'post_data', 'author__avatar', 'author__first_name',
                                          'author__last_name')
        context['post_wall'] = paginator(qs, 20, self.page)
        context['are_friends'] = User.managers.are_friends(kwargs['user_id'], self.request.user)
        return context

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            self.form.save()
            messages.success(request, _(u'успешно опобликировано сообщения'))
            redirect(request.path)
        return self.get(request, *args, **kwargs)


def login_view(request):
    if request.user.is_authenticated():
        return redirect('main')
    if 'POST' in request.method:
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        user = authenticate(email=email, password=password)
        if user is not None and user.is_active and user.confirmed_registration:
            login(request, user)
            url = request.GET.get('next', None) or request.POST.get('next', None) or 'main'
            return redirect(url)
        else:
            messages.error(request, _(u'email или пароль неправильний'))
    return render(request, 'login.html')


class RegistrationView(TemplateView):
    template_name = 'registration.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('main')
        if request.path in reverse('registration'):
            self.form = RegistrationForm(request.POST or None)
        else:
            value = request.path.split('/')[-1]
            try:
                pk = Signer(salt='registration-confirm').unsign(value)
            except BadSignature:
                raise Http404('404')
            obj = get_object_or_404(User, pk=pk)
            if obj.confirmed_registration:
                raise Http404('404')
            obj.confirmed_registration = True
            obj.save()
            messages.success(request, _(u'регистрация успешно подтверждена и вам нужно авторизоваться'))
            return redirect('login')
        return super(RegistrationView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['form'] = self.form
        return context

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            try:
                obj = self.form.save()
            except ValueError as e:
                messages.error(request, e)
                return redirect(request.path)
            signer = Signer(salt='registration-confirm').sign(obj.pk)
            reg_url = request.build_absolute_uri(reverse('registration', args=signer))
            obj.email_user(subject=(u'регистрация аккаунта'),
                           message=_(u'Здравствуйте, вам нужно подтвердить регистрацию {}  по даном посиланню'.format(reg_url)))
            return render(request, 'send_email.html', {'mess': _(u'ви успешно зарегистриловалися и необходимо пройти по'
                                                                 u' ссылке которая отправлена на емайл для потверджения'
                                                                 u' регистриций')})
        else:
            return self.get(self, request, args, kwargs)


class PasswordRecoveryView(TemplateView):
    template_name = 'password_recovery.html'

    def dispatch(self, request, *args, **kwargs):
        self.form = CustomPasswordResetForum(request.POST or None)
        return super(PasswordRecoveryView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PasswordRecoveryView, self).get_context_data(**kwargs)
        context['form'] = self.form
        context['change_pass'] = True
        return context

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            user = self.form.get_user()
            signer = TimestampSigner(salt='password-recovery-confirm', sep=';').\
                sign(u'{}${}'.format(user.pk, user.last_login))
            reg_url = request.build_absolute_uri(reverse('pass_recovery', args=signer))
            user.email_user(_(u'Восстановление пароля'),
                            'Hellow you need confirm registration {}  for this link'.format(reg_url))
            return render(request, 'send_email.html', {'mess': _(u'пройти по ссылке, которая отправлена на email')})
        else:
            return self.get(self, request, args, kwargs)


class PasswordRecoveryConfirmView(TemplateView):
    template_name = 'password_recovery.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            pk, data = TimestampSigner(salt='password-recovery-confirm', sep=';').\
                unsign(kwargs['token'], max_age=172800).split('$')  # max_age 48 hours
        except (BadSignature, SignatureExpired):
            raise Http404('404')
        self.user = get_object_or_404(User, pk=pk)
        if str(self.user.last_login) != data:
            raise Http404('404')
        if not self.user.confirmed_registration:
            self.user.confirmed_registration = True
        self.form = SetPasswordForm(self.user, request.POST or None)
        return super(PasswordRecoveryConfirmView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PasswordRecoveryConfirmView, self).get_context_data(**kwargs)
        context['form'] = self.form
        return context

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            self.form.save()
            user = authenticate(email=self.form.user.email, password=self.form.cleaned_data['new_password2'])
            login(request, user)
            messages.success(request, _(u'ви успешно изменили пароль'))
            return redirect('settings')
        else:
            return self.get(self, request, args, kwargs)


def _get_form(request, formcls, prefix, user=None, instance=None):
    data = request.POST if prefix in request.POST else None
    if user:
        return formcls(data, prefix=prefix, user=user)
    elif instance:
        return formcls(data, request.FILES or None, prefix=prefix, instance=instance)
    else:
        return formcls(data, prefix=prefix)


class MyView(TemplateView):
    template_name = 'settings.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('main')
        self.user = request.user
        if 'action' in request.POST:
            self.aform = _get_form(request, UserProfile, 'aform_pre', instance=self.user)
            self.bform = _get_form(request, UserPasswordChangeForm, 'bform_pre', user=self.user)
            self.dform = _get_form(request, ChangeEmailForm, 'dform_pre', user=self.user)
        else:
            self.aform = UserProfile(request.FILES or None, prefix='aform_pre', instance=self.user)
            self.bform = UserPasswordChangeForm(None, user=self.user, prefix='bform_pre')
            self.dform = ChangeEmailForm(user=self.user, prefix='dform_pre')
        return super(MyView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MyView, self).get_context_data(**kwargs)
        context['aform'] = self.aform
        context['bform'] = self.bform
        context['dform'] = self.dform
        return context

    def post(self, request, *args, **kwargs):
        if 'action' in request.POST:
            if request.POST['action'] == 'change_user' and self.aform.is_valid():
                if 'avatar' in request.POST and request.POST['avatar']:
                    User.objects.get(pk=self.user.pk).avatar.delete()
                self.aform.save()
                messages.success(request, _(u'ви успешно изменили user'))
            elif request.POST['action'] == 'change_password' and self.bform.is_valid():
                self.bform.save()
                request.user.backend = request.session[BACKEND_SESSION_KEY]
                login(request, request.user)
                messages.success(request, _(u'ви успешно изменили пароль'))
                return redirect(request.path)
            elif request.POST['action'] == 'change_email' and self.dform.is_valid():
                self.dform.save()
                messages.success(request, _(u'ви успешно изменили email'))
            return self.render_to_response({'aform': self.aform, 'bform': self.bform, 'dform': self.dform})
        return self.get(request, *args, **kwargs)


class SearchView(TemplateView):
    template_name = 'search.html'

    def dispatch(self, request, *args, **kwargs):
        self.is_form = request.GET or None
        self.page = request.GET.get('page', 1)
        self.form = SearchForm(request.GET or None)
        return super(SearchView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['form'] = self.form
        return context

    def query_search(self, cleaned_data):
        kwargs = cleaned_data
        qs = User.objects
        if kwargs['first_name'] and kwargs['last_name'] and kwargs['first_name'] != kwargs['last_name']:
            qs = qs.filter(first_name__icontains=kwargs['first_name'], last_name__icontains=kwargs['last_name'])
        elif kwargs['first_name'] and kwargs['first_name'] == kwargs['last_name']:
            qs = qs.filter(Q(first_name__icontains=kwargs['first_name']) | Q(last_name__icontains=kwargs['first_name']))
        if User.MEN in str(kwargs['sex']) or User.WOMEN in str(kwargs['sex']):
            qs = qs.filter(sex=kwargs['sex'])
        if kwargs['start_year']:
            qs = qs.extra(where=["strftime('%Y', birthday) >= %s"], params=[str(kwargs['start_year'])])
        if kwargs['end_year']:
            qs = qs.extra(where=["strftime('%Y', birthday) < %s"], params=[str(int(kwargs['end_year']) + 1)])
        if kwargs['city']:
            qs = qs.filter(city__icontains=kwargs['city'])
        if kwargs['work_place']:
            qs = qs.filter(work_place__icontains=kwargs['work_place'])
        if kwargs['about']:
            qs = qs.filter(about__icontains=kwargs['about'])
        if kwargs['interests']:
            qs = qs.filter(interests__icontains=kwargs['interests'])
        if qs.all():
            return qs
        return None

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        spl = request.META['QUERY_STRING'].find('names=')
        if spl != -1:
            context['get'] = request.META['QUERY_STRING'][spl:]
        if self.is_form and self.form.is_valid():
            self.qs = self.query_search(self.form.cleaned_data)
            if self.qs:
                self.qs.only('pk', 'avatar', 'first_name', 'last_name', 'city' 'birthday')
                context['users'] = paginator(self.qs.all(), 20, self.page)
            elif any(value for key, value in self.form.cleaned_data.iteritems() if value and value != u'A' and key
                                                                                   not in ('first_name', 'last_name')):
                messages.error(request, _(u'По заданным критериям не найдено ни одного пользователя'))
                redirect(request.path)
        elif self.form.errors:
            messages.error(request, _(u'something error'))
            redirect(request.path)
        elif 'first_name' in request.GET:
            context['users'] = paginator(User.objects.all(), 20, self.page)
        return self.render_to_response(context)


class MessagesView(TemplateView):
    template_name = 'messages.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        if 'user_id' in kwargs:
            self.form = MessageForm(request.POST or None)
            self.recipient = get_object_or_404(User, pk=kwargs['user_id'])
        return super(MessagesView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        if hasattr(self, 'form'):
            context['form'] = self.form
        users_pk = Dialog.get_dialog.get_list_dialog(self.user)
        context['users_pk'] = users_pk
        dialog_qs = User.objects.filter(pk__in=users_pk).only('avatar', 'pk', 'first_name', 'last_name')
        context['dialogs'] = paginator(dialog_qs, 20, self.request.GET.get('usr_page'))
        if 'user_id' in kwargs:
            context['recipient'] = self.recipient
            context['usr_messages'] = paginator(Dialog.get_dialog.get_messages(self.user, kwargs['user_id']),
                                                20, self.request.GET.get('usr_message'))
        return context

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            usr_message = self.form.save()
            Dialog.get_dialog.create(author=self.user, recipient=self.recipient, messages=usr_message)
        return self.get(request, *args, **kwargs)


class WallView(TemplateView):
    template_name = 'wall.html'

    def get_context_data(self, **kwargs):
        context = super(WallView, self).get_context_data(**kwargs)
        qs = User.objects.filter(pk=self.request.user.pk).prefetch_related('wall').values_list('wall', flat=True)
        context['wall'] = paginator(Wall.objects.filter(pk__in=qs), 20, self.request.GET.get('page'))
        context['friendship'] = Wall.FRIENDSHIP
        context['not_friendship'] = Wall.NOT_FRIENDSHIP
        context['wrote'] = Wall.WROTE
        context['wrote_other_user'] = Wall.WROTE_OTHER_USER
        return context
