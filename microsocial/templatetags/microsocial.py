# coding=utf-8
from django import template
from django.templatetags.static import static


register = template.Library()


@register.inclusion_tag('microsocial/tags/paginator.html')
def show_paginator(page, page_arg_name='page', request_get=None):
    return {
        'page': page,
        'page_arg_name': page_arg_name,
        'request_get': request_get,
    }


@register.filter
def get_avatar(user):
    try:
        return user.avatar.url
    except ValueError:
        return static('default_ava.png')


@register.inclusion_tag('microsocial/tags/form_fields_erros.html')
def show_form_field_errors(fields_errors, block_class=None):
    return {
        'errors': fields_errors,
        'block_class': block_class,
    }
