# coding=utf-8
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from microsocial import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^(en/about/)$', flatpage, name='about'),
    url(r'^(en/advertising/)$', flatpage, name='advertising'),
    url(r'^(en/developers/)$', flatpage, name='developers'),
    url(r'^(en/help/)$', flatpage, name='help'),
    url(r'^(en/vacancies/)$', flatpage, name='vacancies'),
    url(r'^(ru/about/)$', flatpage, name='about'),
    url(r'^(ru/advertising/)$', flatpage, name='advertising'),
    url(r'^(ru/developers/)$', flatpage, name='developers'),
    url(r'^(ru/help/)$', flatpage, name='help'),
    url(r'^(ru/vacancies/)$', flatpage, name='vacancies'),
    url(r'^(uk/about/)$', flatpage, name='about'),
    url(r'^(uk/advertising/)$', flatpage, name='advertising'),
    url(r'^(uk/developers/)$', flatpage, name='developers'),
    url(r'^(uk/help/)$', flatpage, name='help'),
    url(r'^(uk/vacancies/)$', flatpage, name='vacancies'),

    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^friends/', include('friends.urls')),
    url(r'', include('user.urls')),
    url(r'^$', views.main, name='main'),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
